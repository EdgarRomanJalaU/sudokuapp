#!/bin/bash

echo "Check Coverage"

COVERAGE_REPORT_PATH="./coverage/coverage.cobertura.xml"

RATE_REGEX='[0-1]\d*(\.\d+)?'

RATE=$( awk 'NR==2' $COVERAGE_REPORT_PATH | grep -Pio $RATE_REGEX | sed -n '1p')

# Represents 100% of coverage
MAX_COVERAGE="1"

if [ "$RATE" = "$MAX_COVERAGE" ];
then
    # Line used to capture coverage in pipeline
    echo "Code coverage: 100%"
    echo "Pass Successful"
    exit 0
fi

# Represents 80% of coverage
MIN_COVERAGE="80"

if [ "$RATE" = 0 ];
then
    # Line used to capture coverage in pipeline
    echo "Code coverage: 00%"
    echo "Minimum coverage to pass check: $MIN_COVERAGE"
    echo "Fail Coverage Check!"
    exit 1
fi

# Removes 0. and trims to 2 digits max. Then if only one digit adds a zero at the end"
COVERAGE_VALUE=$( echo $RATE | tr "." "\n" | sed -n '2p' | cut -c1-2 | sed -e 's/^[1-9]$/&0/' )

# Line used to capture coverage in pipeline
echo "Code coverage: $COVERAGE_VALUE%"

if [ "$COVERAGE_VALUE" -ge "$MIN_COVERAGE" ];
then
    echo "Pass Successful"
    exit 0
fi

echo "Minimum coverage to pass check: $MIN_COVERAGE%"
echo "Fail Coverage Check!"

exit 1
