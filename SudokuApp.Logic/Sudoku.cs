﻿namespace SudokuApp.Logic
{
    /// <summary>
    /// Class to checks if a given array is a solved sudoku.
    /// </summary>
    public static class Sudoku
    {
        /// <summary>
        /// Size of each side of a block of the sudoku.
        /// </summary>
        private const int BlockSideSize = 3;

        /// <summary>
        /// Checks if the given array 9x9 is a sudoku correctly solved.
        /// </summary>
        /// <param name="board">Integer array 9x9 to be checked.</param>
        /// <returns>Returns true if is solved. Returns false otherwise.</returns>
        public static bool IsSolved(int[,] board)
        {
            // Test change to see coverage report in merge request
            if (!IsValid(board))
            {
                return false;
            }

            for (int i = 0; i < 9; i++)
            {
                if (HasRepeats(GetRow(board, i)))
                {
                    return false;
                }
                if (HasRepeats(GetColumn(board, i)))
                {
                    return false;
                }
                if (HasRepeats(GetBlock(board, i)))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the given array is a valid completed sudoku.
        /// </summary>
        /// <param name="board">Integer array to be checked.</param>
        /// <returns>Returns true if is valid. Returns false otherwise.</returns>
        /// <exception cref="ArgumentException"></exception>
        public static bool IsValid(int[,] board)
        {
            try
            {
                if (board.Length != 81)
                {
                    throw new ArgumentException("Size of board has to be 9x9.", nameof(board));
                }
                string fillCheck = CheckFill(board);
                if (fillCheck != string.Empty)
                {
                    throw new ArgumentException(fillCheck, nameof(board));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks for blank spaces or incorrect digits in the int[,] array.
        /// </summary>
        /// <param name="board">Integer array 9x9 to be checked.</param>
        /// <returns>Returns an empty string if correctly filled.
        /// Otherwise returns an string describing the problem.</returns>
        private static string CheckFill(int[,] board)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (board[i,j] == 0)
                    {
                        return "Board has to be fully filled.";
                    }
                    if (board[i, j] < 1 || board[i, j] > 9)
                    {
                        return "Board can only be filled with numbers between 1 and 9.";
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Check for missing numbers from 1 to 9 in an int[9] array.
        /// </summary>
        /// <param name="section">Int array to check.</param>
        /// <returns>Returns true if one of the digits is missing. 
        /// Returns false if all are present.</returns>
        private static bool HasRepeats(int[] section)
        {
            for (int i = 1; i < 10; i++)
            {
                if(Array.Find(section,x=> x == i) == 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a row of the array based on the index.
        /// </summary>
        /// <param name="array">9x9 array to get the row from.</param>
        /// <param name="rowIndex">Index of the row to get.</param>
        /// <returns>Returns the numbers in the row as an int[] array.</returns>
        private static int[] GetRow(int[,] array, int rowIndex)
        {
            int[] row = new int[array.GetLength(0)];

            for (int i = 0; i < row.Length; i++)
            {
                row[i] = array[rowIndex, i];
            }

            return row;
        }

        /// <summary>
        /// Gets a column of the array based on the index.
        /// </summary>
        /// <param name="array">9x9 array to get the column from.</param>
        /// <param name="colIndex">Index of the column to get.</param>
        /// <returns>Returns the numbers in the column as an int[] array.</returns>
        private static int[] GetColumn(int[,] array, int colIndex)
        {
            int[] column = new int[array.GetLength(0)];

            for (int i = 0; i < column.Length; i++)
            {
                column[i] = array[i, colIndex];
            }

            return column;
        }

        /// <summary>
        /// Gets a block of 3x3 in the array based on the index.
        /// </summary>
        /// <param name="array">9x9 array to get the block from.</param>
        /// <param name="blockIndex">Index of the block of 3x3 to get.</param>
        /// <returns>Returns the numbers in the block as an int[] array.</returns>
        private static int[] GetBlock(int[,] array, int blockIndex)
        {
            int rowBottomIndex = (blockIndex / BlockSideSize) * BlockSideSize;
            int colBottomIndex = (blockIndex % BlockSideSize) * BlockSideSize;

            int[] block = new int[array.GetLength(0)];
            int index = 0;

            for (int rowIndex = rowBottomIndex; rowIndex < rowBottomIndex + 3; rowIndex++)
            {
                for (int colIndex = colBottomIndex; colIndex < colBottomIndex + 3; colIndex++)
                {
                    block[index] = array[rowIndex, colIndex];
                    index++;
                }
            }

            return block;
        }
    }
}
