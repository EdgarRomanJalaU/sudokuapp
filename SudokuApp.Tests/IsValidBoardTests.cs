﻿using SudokuApp.Logic;

namespace SudokuApp.Tests
{
    public class IsValidBoardTests
    {
        [Fact]
        public void Given_Valid_Array_IsValid_Returns_True()
        {
            int[,] validBoard = {
                { 8, 2, 7, 1, 5, 4, 3, 9, 6 },
                { 9, 6, 5, 3, 2, 7, 1, 4, 8 },
                { 3, 4, 1, 6, 8, 9, 7, 5, 2 },
                { 5, 9, 3, 4, 6, 8, 2, 7, 1 },
                { 4, 7, 2, 5, 1, 3, 6, 8, 9 },
                { 6, 1, 8, 9, 7, 2, 4, 3, 5 },
                { 7, 8, 6, 2, 3, 5, 9, 1, 4 },
                { 1, 5, 4, 7, 9, 6, 8, 2, 3 },
                { 2, 3, 9, 8, 4, 1, 5, 6, 7 },
            };

            var result = Sudoku.IsValid(validBoard);

            Assert.True(result);
        }

        [Fact]
        public void Given_Small_Array_IsValid_Returns_False()
        {
            int[,] smallBoard = {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 6, 7, 8 },
            };

            var result = Sudoku.IsValid(smallBoard);

            Assert.False(result);
        }

        [Fact]
        public void Given_Large_Array_IsValid_Returns_False()
        {
            int[,] largeBoard = {
                { 1, 4, 7, 1, 4, 7, 1, 4, 7 },
                { 2, 5, 8, 2, 5, 8, 2, 5, 8 },
                { 3, 6, 9, 3, 6, 9, 3, 6, 9 },
                { 4, 7, 1, 4, 7, 1, 4, 7, 1 },
                { 5, 8, 2, 5, 8, 2, 5, 8, 2 },
                { 6, 9, 3, 6, 9, 3, 6, 9, 3 },
                { 7, 1, 4, 7, 1, 4, 7, 1, 4 },
                { 8, 2, 5, 8, 2, 5, 8, 2, 5 },
                { 9, 3, 6, 9, 3, 6, 9, 3, 6 },
                { 9, 3, 6, 9, 3, 6, 9, 3, 6 },
            };

            var result = Sudoku.IsValid(largeBoard);

            Assert.False(result);
        }

        [Fact]
        public void Given_Unfilled_Array_IsValid_Returns_False()
        {
            int[,] unfilledBoard = {
                { 0, 4, 7, 1, 4, 7, 1, 4, 7 },
                { 2, 5, 8, 2, 5, 8, 2, 5, 8 },
                { 3, 6, 9, 3, 6, 9, 3, 6, 9 },
                { 4, 7, 1, 4, 7, 1, 4, 7, 1 },
                { 5, 8, 2, 5, 8, 2, 5, 8, 2 },
                { 6, 9, 3, 6, 9, 3, 6, 9, 3 },
                { 7, 1, 4, 7, 1, 4, 7, 1, 4 },
                { 8, 2, 5, 8, 2, 5, 8, 2, 5 },
                { 9, 3, 6, 9, 3, 6, 9, 3, 6 },
            };

            var result = Sudoku.IsValid(unfilledBoard);

            Assert.False(result);
        }

        [Fact]
        public void Given_Wrongly_Filled_Array_IsValid_Returns_False()
        {
            int[,] wronglyFilledBoard = {
                { 10, 4, 7, 1, 4, 7, 1, 4, 7 },
                { 2, 5, 8, 2, 5, 8, 2, 5, 8 },
                { 3, 6, 9, 3, 6, 9, 3, 6, 9 },
                { 4, 7, 1, 4, 7, 1, 4, 7, 1 },
                { 5, 8, 2, 5, 8, 2, 5, 8, 2 },
                { 6, 9, 3, 6, 9, 3, 6, 9, 3 },
                { 7, 1, 4, 7, 1, 4, 7, 1, 4 },
                { 8, 2, 5, 8, 2, 5, 8, 2, 5 },
                { 9, 3, 6, 9, 3, 6, 9, 3, 6 },
            };

            var result = Sudoku.IsValid(wronglyFilledBoard);

            Assert.False(result);
        }
    }
}
