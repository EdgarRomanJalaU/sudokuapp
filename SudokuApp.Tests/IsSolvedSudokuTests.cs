using SudokuApp.Logic;

namespace SudokuApp.Tests
{
    public class IsSolvedSudokuTests
    {
        [Fact]
        public void Given_Solved_Array_IsSolved_Returns_True()
        {
            int[,] correctBoard = {
                { 8, 2, 7, 1, 5, 4, 3, 9, 6 },
                { 9, 6, 5, 3, 2, 7, 1, 4, 8 },
                { 3, 4, 1, 6, 8, 9, 7, 5, 2 },
                { 5, 9, 3, 4, 6, 8, 2, 7, 1 },
                { 4, 7, 2, 5, 1, 3, 6, 8, 9 },
                { 6, 1, 8, 9, 7, 2, 4, 3, 5 },
                { 7, 8, 6, 2, 3, 5, 9, 1, 4 },
                { 1, 5, 4, 7, 9, 6, 8, 2, 3 },
                { 2, 3, 9, 8, 4, 1, 5, 6, 7 },
            };

            var result = Sudoku.IsSolved(correctBoard);

            Assert.True(result);
        }

        [Fact]
        public void Given_Array_With_Repeated_Numbers_In_A_Row_IsSolved_Returns_False()
        {
            int[,] repeatedInRow = {
                { 1, 4, 7, 1, 4, 7, 1, 4, 7 },
                { 2, 5, 8, 2, 5, 8, 2, 5, 8 },
                { 3, 6, 9, 3, 6, 9, 3, 6, 9 },
                { 4, 7, 1, 4, 7, 1, 4, 7, 1 },
                { 5, 8, 2, 5, 8, 2, 5, 8, 2 },
                { 6, 9, 3, 6, 9, 3, 6, 9, 3 },
                { 7, 1, 4, 7, 1, 4, 7, 1, 4 },
                { 8, 2, 5, 8, 2, 5, 8, 2, 5 },
                { 9, 3, 6, 9, 3, 6, 9, 3, 6 },
            };

            var result = Sudoku.IsSolved(repeatedInRow);

            Assert.False(result);
        }

        [Fact]
        public void Given_Array_With_Repeated_Numbers_In_A_Column_IsSolved_Returns_False()
        {
            int[,] repeatedInCol = {
                { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                { 4, 5, 6, 7, 8, 9, 1, 2, 3 },
                { 7, 8, 9, 1, 2, 3, 4, 5, 6 },
                { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                { 4, 5, 6, 7, 8, 9, 1, 2, 3 },
                { 7, 8, 9, 1, 2, 3, 4, 5, 6 },
                { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                { 4, 5, 6, 7, 8, 9, 1, 2, 3 },
                { 7, 8, 9, 1, 2, 3, 4, 5, 6 },
            };

            var result = Sudoku.IsSolved(repeatedInCol);

            Assert.False(result);
        }

        [Fact]
        public void Given_Array_With_Repeated_Numbers_In_A_Block_IsSolved_Returns_False()
        {
            int[,] repeatedInBlock = {
                { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                { 2, 3, 4, 5, 6, 7, 8, 9, 1 },
                { 3, 4, 5, 6, 7, 8, 9, 1, 2 },
                { 4, 5, 6, 7, 8, 9, 1, 2, 3 },
                { 5, 6, 7, 8, 9, 1, 2, 3, 4 },
                { 6, 7, 8, 9, 1, 2, 3, 4, 5 },
                { 7, 8, 9, 1, 2, 3, 4, 5, 6 },
                { 8, 9, 1, 2, 3, 4, 5, 6, 7 },
                { 9, 1, 2, 3, 4, 5, 6, 7, 8 },
            };

            var result = Sudoku.IsSolved(repeatedInBlock);

            Assert.False(result);
        }

        [Fact]
        public void Given_Invalid_Array_IsSolved_Returns_True()
        {
            int[,] correctBoard = {
                { -1, 2, 7, 1, 5, 4, 3, 9, 6 },
                { 9, 6, 5, 3, 2, 7, 1, 4, 8 },
                { 3, 4, 1, 6, 8, 9, 7, 5, 2 },
                { 5, 9, 3, 4, 6, 8, 2, 7, 1 },
                { 4, 7, 2, 5, 1, 3, 6, 8, 9 },
                { 6, 1, 8, 9, 7, 2, 4, 3, 5 },
                { 7, 8, 6, 2, 3, 5, 9, 1, 4 },
                { 1, 5, 4, 7, 9, 6, 8, 2, 3 },
                { 2, 3, 9, 8, 4, 1, 5, 6, 7 },
            };

            var result = Sudoku.IsSolved(correctBoard);

            Assert.False(result);
        }
    }
}